﻿CREATE TABLE cookies(
    key character(36) PRIMARY KEY, 
    value character(36) NOT NULL
);


CREATE TABLE users(
    id serial PRIMARY KEY, 
    name character(70) NOT NULL,
    username character(70) NOT NULL,
    age integer NOT NULL, 
    password character(20) NOT NULL
);

CREATE TABLE jobs(
    id serial PRIMARY KEY, 
    job_name character(70) NOT NULL,
    ocupation character(70) NOT NULL,
    date_in date NOT NULL,
    date_off date NOT NULL,
    user_id integer NOT NULL,
    CONSTRAINT fk_user 
      FOREIGN KEY (user_id)
      REFERENCES users(id)
);

INSERT INTO users (name, age, username, password) 
  VALUES ('Pedro Konstantinoff',36,'pedro','un_password'); 

SELECT * FROM users;
