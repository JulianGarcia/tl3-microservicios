from flask import (
    Flask,
    render_template,
    make_response,
    jsonify,
    request,
)
from http import cookies
from sqlalchemy import create_engine
from sqlalchemy import Table, Column, String, Integer, MetaData, DateTime, ForeignKey, tuple_, select
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
import logging

logger = logging.getLogger()

app = Flask(__name__)

PORT = 7000
HOST = '0.0.0.0'

db_string = "postgres://admin:admin@postgres:5432/microservices"
engine = create_engine(db_string, echo=False)

metadata = MetaData()
metadata.create_all(engine) 
Session = sessionmaker(bind=engine)
session = Session()


class User(declarative_base()):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    username = Column(String)
    age = Column(Integer)
    password = Column(String)

    def __init__(self, name, age, username, password):
        self.name = name
        self.age = age
        self.username = username
        self.password = password

#microservice endpoints
@app.route("/")
def index():
    return render_template('index.html')

@app.route("/users", methods=["GET"])
def query_users():
    users = []
    for u in session.query(User).all():
        user = u.__dict__
        user.pop('_sa_instance_state', None)    
        users.append(user)
    return jsonify(users)


@app.route("/users", methods=["POST"])
def create_user():
    try:
        form = request.get_json(force = True)
        user = User(
            form.get('name'),
            form.get('age'),
            form.get('username'),
            form.get('password')
        )
        print(user)
        session.add(user)
        session.commit()
        return {'error': False}
    except:
        return {'error': True}

if __name__ == '__main__':
    print(f"Users microservice runnning in port {PORT}")
    app.run(host=HOST, port=PORT, debug = True)