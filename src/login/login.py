from flask import (
    Flask, 
    render_template, 
    make_response, 
    jsonify, 
    request,
)
from http import cookies
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Table, Column, String, Integer, MetaData, DateTime, ForeignKey, tuple_
import logging

logger = logging.getLogger()

app = Flask(__name__)

PORT = 5000
HOST = '0.0.0.0'


class Database():
    __instance = None

    @staticmethod
    def instance():
        if (Database.__instance == None):
            Database.__instance = Database()
        return Database.__instance

    def __init__(self):
        if (Database.__instance is not None):
            return Database.__instance
        db_string = "postgres://admin:admin@postgres:5432/microservices"
        self.db = create_engine(db_string, echo=False)
        self.meta = MetaData(self.db)

        self.cookies = Table(
                        'cookies', 
                        self.meta, 
                        Column('key', String, primary_key=True),
                        Column('value', String)
                       )

        self.users = Table(
                        'users',
                        self.meta,
                        Column('id', Integer, primary_key=True),
                        Column('name', String),
                        Column('username', String),
                        Column('age', Integer),
                        Column('password', String),
                        )

    def exists_cookie(self, key, value):
        result = self.meta.tables["cookies"]
        cookie_row = result.select().where(result.c.key==key)
        cookies_rs = self.db.execute(cookie_row)
        return cookies_rs.rowcount == 1

    def get_cookie(self, key):
        result = self.meta.tables["cookies"]
        cookie_row = result.select().where(result.c.key==key)
        cookie_rs = self.db.execute(cookie_row)
        if (cookie_rs.rowcount != 0):
            cookie = cookie_rs.first()
            return [cookie[0],cookie[1]]
        else:
            return []

    def insert_cookie(self, key, value):
        statement = self.cookies.insert().values(key=key, value=value)
        self.db.execute(statement)

    def check_credentials(self, username, password):
        users = self.meta.tables["users"]
        user = self.db.users.filter(and_(users.username==username, users.password==password))
        result = self.db.execute(user)
        hay = result.query.count() 
        if hay:
            return "{'existe': true}"
        else:
            return "{'existe': false}"
    
    def find_user(self, user):
        users_table = self.meta.tables["users"]
        _user = self.users.filter(users.username==user)
        result = self.db.execute(_user)
        logger.exception(result)
        if (result.rowcount != 0):
            ret_user = result.first()
            return ret_user
        else:
            return "not user found"

db = Database()

#microservice endpoints
@app.route("/login")
def login():
    return render_template('login.html')


@app.route("/login")
def authenticate_user():
    response = {"success": False, "falla": "antes de preguntar si es post o get"}
    if request.method=='POST':
        #existe el usuario?
        req_cookies = request.cookies
        cookie_session_key = req_cookies.get('session_key')
        cookie_session_value = req_cookies.get('session_value')

        if db.check_credentials(cookie_session_key, cookie_session_value):
            #tiene cookie en la db?
            if not db.exists_cookie(cookie_session_key, cookie_session_value):
                #no, le creo una
                db.insert_cookie(cookie_session_key, cookie_session_value)
            #lo logeo y redirecciono
            response = {"success": True, "falla": "none"}
        else:
            response = {"success": False, "falla": "try"}
    if request.method == 'GET':
        req_cookies = request.cookies
        cookie_session_key = req_cookies.get('session_key')
        cookie_session_value = req_cookies.get('session_value')

        if db.check_credentials(cookie_session_key, cookie_session_value):
            response = {"success": True, "falla": "YA ESTA AUTENTICADO"}
        
    return response


if __name__ == '__main__':
    print(f"Login microservice runnning in port {PORT}")
    app.run(host=HOST, port=PORT)