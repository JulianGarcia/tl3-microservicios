#!/usr/bin/python3
from flask import (
    Flask,
    render_template,
    make_response,
    jsonify,
    request,
)
from http import cookies
from sqlalchemy import create_engine
from sqlalchemy import Table, Column, String, Integer, MetaData, DateTime, ForeignKey, tuple_, select
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
import logging
import datetime

logger = logging.getLogger()
app = Flask(__name__)

PORT = 6000
HOST = '0.0.0.0'

db_string = "postgres://admin:admin@postgres:5432/microservices"
engine = create_engine(db_string, echo=False)

metadata = MetaData()
metadata.create_all(engine) 
Session = sessionmaker(bind=engine)
session = Session()

class Job(declarative_base()):
    __tablename__ = 'jobs'

    id = Column(Integer, primary_key=True)
    job_name = Column(String)
    ocupation = Column(String)
    date_in = Column(DateTime)
    date_off = Column(DateTime)
    user_id = Column(Integer)

    def __init__(self, job_name, ocupation, date_in, date_off, user_id):
        self.job_name= job_name
        self.ocupation = ocupation
        self.date_in = date_in
        self.date_off = date_off
        self.user_id = user_id


@app.route("/user_view")
def user_view():
    return render_template('user_view.html')

@app.route("/jobs", methods=["GET"])
def query_jobs():
    jobs = []
    for j in session.query(Job).all():
        job = j.__dict__
        job.pop('_sa_instance_state', None)    
        jobs.append(job)
    logger.error(jobs)
    return jsonify(jobs)


@app.route("/jobs", methods=["POST"])
def create_job():
    try:
        form = request.get_json(force= True)
        job = Job(
            form.get('job_name'),
            form.get('ocupation'),
            form.get('date_in'),
            form.get('date_off'),
            1
            )
        session.add(job)
        session.commit()
        return {'error': False}
    except:
        return {'error': True}



if __name__ == '__main__':
    print(f"JOBS microservice runnning in port {PORT}")
    app.run(host=HOST, port=PORT, debug = True)